#!/usr/bin/env python3

import socket as s
import traceback
import os
import fcntl
from importlib import reload
import rigol_functions as rf

sck = s.socket(s.AF_INET, s.SOCK_STREAM)
sck.connect(('192.168.0.18',5555))
fcntl.fcntl(sck,fcntl.F_SETFL, os.O_NONBLOCK)
    
while True:
    """
    Start a REPL, intercepting some 'special' commands
    and passing everything else through.
    """
    st = input('rigol>').strip()

    try:
        if st == "s":
            rf.send(sck,b':SINGLE\r\n')
            continue
        elif st[:3] == 'scl':
            rf.channel_offset(sck,st)
            continue
        elif st[:7] == 'plotraw':
            rf.plotraw(sck,st)
            continue
        elif st[:4] == 'plot':
            rf.plot(sck,st)
            continue
        elif st == 'quit':
            break
        elif st == 'reload':
            rf = reload(rf)
        else:
            rf.send(sck,'%s\r\n'%st)
        r = rf.get(sck)
        if r is not None:
            print(r)
    except:
        print('Error in command:')
        traceback.print_exc()
