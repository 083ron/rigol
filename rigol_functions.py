import time
import matplotlib.pyplot as p
from collections import namedtuple

Preamble = namedtuple('Preamble', 'format mode npts count xinc xorg xref yinc yorg yref')

def send(sck, st):
    if type(st) == str:
        b = st.encode()
    else:
        b = st
        
    sck.send(b)

def get(sck):
    time.sleep(.2)
    try:
        msg = b''
        while 1:
            msg += sck.recv(4096)
    except BlockingIOError as e:
        pass
    except Exception as e:
        print( e, type(e))
    finally:
        return msg

def get_data_preamble(sck):
    send(sck,':WAV:PRE?\r\n')

    r = get(sck)
    pre = None
    if r is not None:
        bits = r.decode('ascii').split(',')
        
        pre = Preamble([int(a) for a in bits[:4]] +
                       [float(a) for a in bits[4:]])
    return pre
    
def channel_offset(sck,cmd):
    chn = int(cmd[3:])
    print( ':CHAN%d:OFFS?\r\n'%chn)
    send(sck,b':CHAN%d:OFFS?\r\n'%chn)
    r = get(sck)
    if r is None: return None
    r = r.decode('ascii').strip()
    print("R: ", r)
    for i in range(1,5):
        time.sleep(.2)
        print( ':CHAN%d:OFFS %s\r\n'%(i,r))
        send(sck,b':CHAN%d:OFFS %s\r\n'%(i,r.encode('ascii')))

def plotraw(sck,st):
    chn = int(st.split()[1])
    print(':WAV:SOUR CHAN%d\r\n'%chn)
    send(sck,b':WAV:SOUR CHAN%d\r\n'%chn)
    time.sleep(.2)
    send(sck,b':WAV:MODE RAW\r\n')
    time.sleep(.2)
    send(sck,b':WAV:FORM BYTE\r\n')
    time.sleep(.2)
    pre = get_deta_preamble(sck)
    
    nbatches = max(1, pre.npts//250000)
    nperbatch = (pre.npts//nbatches)

    print (nbatches, nperbatch)
    
    ys = []
    for i in range(nbatches):
        print(i,"/",nbatches)
        send(sck,":WAV:START %d\r\n"%((i*nperbatch)+1))
        send(sck,':WAV:STOP %d\r\n'%((i+1)*nperbatch))
        send(sck,b':WAV:DATA?\r\n')
        time.sleep(.5)
        r = get(sck)
        if r is not None:
            dt = [int(a) for a in r[11:]]     
            ys += dt
            print(dt[0], 0)
            print(dt[-1], -1)
            
    xs = range(len(ys))
    showplot(xs,ys)

def plot(sck,st):
    chn = int(st.split()[1])
    send(sck,b':WAV:SOUR CHAN%d\r\n'%chn)
    time.sleep(.2)
    send(sck,b':WAV:MODE NORM\r\n:WAV:FORM BYTE\r\n:WAV:DATA?\r\n')
    r = get(sck)
    print (r[-4:])
    if r is not None:
        ys = [int(a) for a in r[11:-1]]
    xs = range(len(ys))
    showplot(xs,ys)

def showplot(xs,ys):
    p.plot(xs,ys)
    fig = p.gcf()
    fig.set_size_inches(12,7)
    p.show()
